import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class Assignment {
    private JButton aButton;
    private JButton bButton;
    private JButton dButton;
    private JButton eButton;
    private JButton cButton;
    private JButton fButton;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JPanel root;
    private JLabel total;
    private JLabel yen;
    private JTextPane priceList;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    //合計金額の定義
    int sumPrice = 0;

    void order(String food,int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
            String currentOrderedText = orderedItemsList.getText();
            String currentPriceText = priceList.getText();
            orderedItemsList.setText(currentOrderedText + food + "\n");
            priceList.setText(currentPriceText + price + " yen\n");
            sumPrice += price;
            yen.setText(sumPrice + " yen");
        }
    }


    public Assignment() {
        aButton.setIcon(new ImageIcon(this.getClass().getResource("sushi.jpg")));
        aButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",900);
            }
        });

        bButton.setIcon(new ImageIcon(this.getClass().getResource("stake.jpg")));
        bButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Stake", 800);
            }
        });

        cButton.setIcon(new ImageIcon(this.getClass().getResource("pizza.jpg")));
        cButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pizza", 700);
            }
        });

        dButton.setIcon(new ImageIcon(this.getClass().getResource("pasta.jpg")));
        dButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pasta", 600);
            }
        });

        eButton.setIcon(new ImageIcon(this.getClass().getResource("hamburger.jpg")));
        eButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburger", 500);
            }
        });

        fButton.setIcon(new ImageIcon(this.getClass().getResource("cake.jpg")));
        fButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cake", 400);
            }
        });


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout?",
                            "Checkout confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if (confirmation == 0) {
                        if(sumPrice <= 0) {
                            JOptionPane.showMessageDialog(null,
                                    "You have not ordered anything yet.");
                        } else {
                            JOptionPane.showMessageDialog(null, "Thank you! The total price is "
                                    + sumPrice + " yen.");
                            orderedItemsList.setText("\n");
                            priceList.setText("\n");
                            sumPrice = 0;
                            yen.setText(sumPrice + " yen");
                        }
                    }
            }
        });

    }
}
